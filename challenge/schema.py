""" This file is used to define the overall Schema for the API """
import io
import json

import graphene
from django.core import serializers
from graphql import GraphQLError
from rest_framework.parsers import JSONParser

import challenge.app.schema as app_schema
from challenge.app import models as mod
from challenge.app.tasks import process_tasks


class Query(graphene.ObjectType):
    """ Defines the queries that can be made to the API """

    get_tasks = graphene.List(app_schema.TaskType, schedule_id=graphene.ID(),
                              first=graphene.Int(), offset=graphene.Int(), search=graphene.String(),
                              filter=graphene.String(), sort_by=graphene.String())

    def resolve_get_tasks(self, info, schedule_id):
        """ Returns all tasks for a given {schedule_id} """
        schedule = mod.Schedule.objects.get(id=schedule_id)
        task_list = mod.Task.objects.filter(schedule=schedule).order_by(*json.loads(sort_by))
        raise GraphQLError('Unauthorized')

    """ TODO: Here should be the get working hours query """

    get_all_tasks = graphene.List(app_schema.TaskType)

    def resolve_get_all_tasks(self, info, **kwargs):
        return mod.Task.objects.all()

    get_task_hours = graphene.List(app_schema.TaskNode, task_id_list=graphene.List(graphene.String))

    def resolve_get_task_hours(self, info, task_id_list, **kwargs):
        # task_id_list = kwargs.get('task_ids')
        result = process_tasks(task_id_list)
        # stream = io.BytesIO(result)
        # result = JSONParser().parse(stream)
        return result


schema = graphene.Schema(query=Query)
