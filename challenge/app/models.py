import uuid
from datetime import timedelta, datetime, date

from django.db import models
# Create your models here.
from rest_framework import serializers

from challenge.app.tasks import get_hours


class Project(models.Model):
    """ Project model """

    name = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now=True)
    uid = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    """ Schedule model """

    name = models.CharField(max_length=100)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True,
                                related_name="schedules")
    status = models.CharField(max_length=50, default="New")
    schedule_file = models.FileField(upload_to='schedule_files/', default=None)
    deadline = models.DateTimeField(default=None, null=True)


class Calendar(models.Model):
    """ Calendar model """

    name = models.CharField(max_length=100)
    calendar_id = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    def get_working_hour_on_day(self, start=None):
        total = timedelta(minutes=0)
        shifts = self.shifts.all()
        for shift in shifts:
            if start and start > shift.end:
                continue
            if not start:
                start = shift.start
            if start < shift.start:
                start = shift.start

            total += datetime.combine(date.min, shift.end) - datetime.combine(date.min, start)

        return total


class Task(models.Model):
    """ Task model """

    name = models.CharField(max_length=200)
    task_id = models.CharField(max_length=20, default="")
    start = models.DateTimeField(default=None, null=True)
    end = models.DateTimeField(default=None)
    calendar = models.ForeignKey(
        Calendar, on_delete=models.DO_NOTHING, default=None, null=True,
        related_name="tasks")

    @property
    def hours(self):
        """
        to return hours by the task
        :return:
        """
        result = get_hours(self.start, self.end)
        return result


class Shift(models.Model):
    """Shift Model: to store the morning and afternoon shifts for each day"""

    start = models.TimeField(null=True, default=None)
    end = models.TimeField(null=True, default=None)
    calender = models.ForeignKey(Calendar, on_delete=models.DO_NOTHING, default=None, null=True, related_name='shifts')


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('task_id','hours')
