import pytest
from graphene.test import Client

from challenge.app.models import Task
from challenge.app.tasks import *
from challenge.schema import schema as my_schema


@pytest.fixture(scope='session')
def django_db_setup():
    settings.DATABASES['default'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'backend-challenge.sqlite',
    }


pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestTasks:
    pytestmark = pytest.mark.django_db

    def test_get_hours_same_week(self):
        s = datetime.datetime.strptime("2013-06-18T08:00:00", '%Y-%m-%dT%H:%M:%S')
        e = datetime.datetime.strptime("2013-06-19T08:00:00", '%Y-%m-%dT%H:%M:%S')
        x = get_hours(s, e)
        assert x == 7

    def test_get_hours_same_day(self):
        s = datetime.datetime.strptime("2013-06-18T08:00:00", '%Y-%m-%dT%H:%M:%S')
        e = datetime.datetime.strptime("2013-06-18T08:00:00", '%Y-%m-%dT%H:%M:%S')
        x = get_hours(s, e)
        assert int(x) == 0

    def test_get_hours_next_week(self):
        s = datetime.datetime.strptime("2013-06-18T08:00:00", '%Y-%m-%dT%H:%M:%S')
        e = datetime.datetime.strptime("2013-06-28T08:00:00", '%Y-%m-%dT%H:%M:%S')
        x = get_hours(s, e)
        assert int(x) == 65

    def test_a_task(self):
        task = Task.objects.filter(task_id='39841').first()
        assert task.hours == 1377.5

    def test_query(self):
        client = Client(my_schema)
        executed = client.execute('''query{
  getTaskHours(taskIdList:["39841"]){
    taskId,
    hours
  }
}''', )
        assert executed == {
            "data": {
                "getTaskHours": [
                    {
                        "hours": 1377.5,
                        "taskId": "39841"
                    }
                ]
            }
        }
