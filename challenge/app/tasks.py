import datetime
import json
import os
import pickle

from django.conf import settings
from django.core import serializers

from challenge.app import models as mod

from challenge.celery import app
from django.forms.models import model_to_dict

""" Function to convert the task dates to working hours """
days_list = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
weekdays = {str(key): val for key, val in enumerate(days_list, 1)}



# @app.task(serializer='json')
# Sorry! Didn't have the time to go after celery properly
def process_tasks(task_id_list):
    # tasks = [model_to_dict(task) for task in mod.Task.objects.filter(task_id__in=task_id_list)]
    tasks = mod.Task.objects.filter(task_id__in=task_id_list)
    return tasks

    # I was looking to combine The same task ids into one but later seemed unnecessary
    # result_dict = {}
    # for task in tasks:
    #     if not result_dict.get(task.task_id):
    #         result_dict[task.task_id] = 0
    #     result_dict[task.task_id] += task.hours
    #
    #
    # result = [{'Taskid': key, 'Hours': val} for key, val in result_dict.items()]
    # return result


def get_hours(start: datetime.datetime, end: datetime.datetime):
    """
    function to get hours from start date to end date
    :param start:
    :param end:
    :return:
    """
    start_day = start.weekday() + 1
    # Max in case it drops to zero
    end_day = max(end.weekday(), 1)

    start_number_of_week = start.isocalendar()[1]
    end_number_of_week = end.isocalendar()[1]
    if start.year < end.year:
        for i in range(start.year, end.year):
            end_number_of_week += 52

    if start_number_of_week == end_number_of_week:
        result = get_weekly_hours(custom_start=start_day, custom_end=end_day)

    else:
        weekly_total = get_weekly_hours()
        # print(weekly_total)
        total = 0
        for i in range(start_number_of_week, end_number_of_week + 1):

            if i == start_number_of_week:
                total += get_weekly_hours(custom_start=start_day)
            elif i == end_number_of_week:
                total += get_weekly_hours(custom_end=end_day)
            else:
                total += weekly_total

            # print(f'{i} - {total}')

        result = total

    return result


def get_weekly_hours(custom_start=None, custom_end=None):
    """

    :param custom_start: Custom start number of the day in week
    :param custom_end: Custom End Number of the day in week
    :return: Total hours in the week
    """
    calenders = mod.Calendar.objects.all()
    total = datetime.timedelta(minutes=0)
    for day in calenders:
        if custom_start and custom_start > int(day.calendar_id):
            continue
        if custom_end and custom_end < int(day.calendar_id):
            break
        total += day.get_working_hour_on_day()
    return total.total_seconds() / 3600

def import_to_db():
    """
    Used this function to import values from fixture to db to test
    :return:
    """
    file_path = os.path.join(settings.BASE_DIR, 'challenge', 'app', 'fixtures', 'date-calendar-seed.json')
    with open(file_path) as file:
        x = json.loads(file.read())

    tasks = x['tasks']
    cals = x['Calendar']

    for item in tasks:
        st = datetime.datetime.strptime(item['start'], '%Y-%m-%dT%H:%M:%S')
        end = datetime.datetime.strptime(item['end'], '%Y-%m-%dT%H:%M:%S')
        task = mod.Task(task_id=item['task_id'], start=st, end=end)
        task.save()

    for key, val in cals.items():
        calender, created = mod.Calendar.objects.get_or_create(name=weekdays[key], calendar_id=key)
        for item in val:
            start = datetime.datetime.strptime(item[0], '%H:%M:%S')
            end = datetime.datetime.strptime(item[1], '%H:%M:%S')
            result, created = calender.shifts.update_or_create(start=start.time(), end=end.time())
