import graphene
from graphene_django import DjangoObjectType

import challenge.app.models as mod


# Write your unit test here using PyTest.

class ProjectType(DjangoObjectType):
    """ Django object type for returning Projects to the user """

    class Meta:
        """ Uses the Project model defined in models.py """
        model = mod.Project


class ScheduleType(DjangoObjectType):
    """ Django object type for returning Schedules to the user """

    class Meta:
        """ Uses the Schedule model defined in models.py """
        model = mod.Schedule


class TaskType(DjangoObjectType):
    """ Django object type for returning Tasks to the user """
    hours = graphene.Float()
    class Meta:
        """ Uses the Task model defined in models.py """
        model = mod.Task

class TaskNode(DjangoObjectType):
    """ Django object type for returning Tasks to the user """
    hours = graphene.Float()
    class Meta:
        """ Uses the Task model defined in models.py """
        model = mod.Task
        only_fields = ('task_id','hours')
